﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace webapi.Controllers
{
    public class FilmesController : ApiController
    {
        DataClasses1DataContext dc = new DataClasses1DataContext();

        // GET: api/Filmes
        public List<Filme> Get()
        {
            var lista = from Filme in dc.Filmes orderby Filme.titulo select Filme;

            return lista.ToList();
        }

        // GET: api/Filmes/5
        public IHttpActionResult Get(int id)
        {
            var filme = dc.Filmes.SingleOrDefault(f => f.id == id);
            
            if(filme != null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, filme));
            }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "Filme não encontrado com este ID"));

        }

        // POST: api/Filmes
        public IHttpActionResult Post([FromBody]Filme novoFilme)
        {
            Filme filme = dc.Filmes.FirstOrDefault(f => f.id == novoFilme.id);

            if(filme != null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.Conflict, "Já existe um filme com este ID"));
            }

            Categoria categoria = dc.Categorias.FirstOrDefault(c => c.Sigla == novoFilme.categoria);

            if(categoria == null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound,
                    "Não existe ainda essa categoria"));
            }

            dc.Filmes.InsertOnSubmit(novoFilme);

            try
            {
                dc.SubmitChanges();
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.ServiceUnavailable, e));
            }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, "Filme criado com sucesso"));

        }

        // PUT: api/Filmes/5
        public IHttpActionResult Put([FromBody]Filme filmeAlterado)
        {
            Filme filme = dc.Filmes.FirstOrDefault(f => f.id == filmeAlterado.id);

            if (filme == null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound,
                    "Não existe nenhum filme com este ID"));
            }

            Categoria categoria = dc.Categorias.FirstOrDefault(c => c.Sigla == filmeAlterado.categoria);

            if (categoria == null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound,
                    "Não existe ainda essa categoria"));
            }

            filme.titulo = filmeAlterado.titulo;
            filme.categoria = filmeAlterado.categoria;

            try
            {
                dc.SubmitChanges();
            }
            catch (Exception e)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.ServiceUnavailable, e));
            }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, "Filme alterado com sucesso"));
        }

        // DELETE: api/Filmes/5
        public IHttpActionResult Delete(int id)
        {
            Filme filme = dc.Filmes.FirstOrDefault(f => f.id == id);

            if (filme != null)
            {
                dc.Filmes.DeleteOnSubmit(filme);

                try
                {
                    dc.SubmitChanges();
                }
                catch (Exception e)
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.ServiceUnavailable, e));
                }

                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK,
                    "Filme eliminado com sucesso"));
            }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "Não existe um filme com este ID"));
        }
    }
}
