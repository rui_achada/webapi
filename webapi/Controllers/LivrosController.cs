﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using webapi.Models;

namespace webapi.Controllers
{
    public class LivrosController : ApiController
    {
        // GET: api/Livros
        public List<Livro> Get()
        {
            return Biblioteca.Livros;
        }

        // GET: api/Livros/5
        public IHttpActionResult Get(int id) // interface
        {
            Livro livro = Biblioteca.Livros.FirstOrDefault(x => x.Id == id);

            if(livro != null) // significa que encontrou o livro
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, livro)); // Status OK
            }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "Livro não localizado"));
        }

        // POST: api/Livros
        public IHttpActionResult Post([FromBody]Livro obj)
        {
            Livro livro = Biblioteca.Livros.FirstOrDefault(x => x.Id == obj.Id);
            // ver primeiro se já existe um livro com este ID, antes de criar

            if (livro != null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.Conflict,
                    "Já existe um livro registado com esse ID"));
            }

            Biblioteca.Livros.Add(obj);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, "Livro inserido com sucesso"));
        }

        // PUT: api/Livros/5
        public IHttpActionResult Put([FromBody]Livro obj)
        {
            Livro livro = Biblioteca.Livros.FirstOrDefault(x => x.Id == obj.Id);

            if (livro != null)
            {
                livro.Titulo = obj.Titulo;
                livro.Autor = obj.Autor;
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, "Alterou com sucesso"));
            }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "Livro não localizado"));
        }

        // DELETE: api/Livros/5
        public IHttpActionResult Delete(int id)
        {
            Livro livro = Biblioteca.Livros.FirstOrDefault(x => x.Id == id);

            if(livro != null)
            {
                Biblioteca.Livros.Remove(livro);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, "Eliminado com sucesso"));
            }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.NotFound, "Livro não localizado"));

        }
    }
}
