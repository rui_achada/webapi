﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace webapi.Models
{
    public class Biblioteca
    {
        private static List<Livro> livros;

        public static List<Livro> Livros
        {
            get
            {
                if(livros==null)
                {
                    GerarLivros();
                }
                return livros;
            }
            set
            {
                livros = value;
            }
        }

        private static void GerarLivros()
        {
            livros = new List<Livro>();

            livros.Add(new Livro
            {
                Id = 1,
                Titulo = "Filme triste",
                Autor = "Rui Mendes"
            });

            livros.Add(new Livro
            {
                Id = 2,
                Titulo = "Filme mais triste",
                Autor = "Rui Outro"
            });

            livros.Add(new Livro
            {
                Id = 3,
                Titulo = "Filme último",
                Autor = "Outro Rui"
            });
        }
    }
}